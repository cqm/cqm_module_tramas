<?php
/**
 * Created by PhpStorm.
 * User: sfreire
 * Date: 12/9/18
 * Time: 16:22
 */

namespace cqm\modules\TRM\Model;

use Doctrine\ORM\Mapping as ORM;


class Parameters
{


    protected $id;

    protected $key;

    protected $value;

    protected $createdBy = 'SYSTEM';

    protected $updatedBy = '';

    protected $createdAt;

    protected $updatedAt;

    public function getId(){
        return $this->id;
    }

    public function getKey(){
        return $this->key;
    }

    public function setKey($key){
        $this->key = $key;
    }

    public function getValue(){
        return $this->value;
    }

    public function setValue($value){
        $this->value = $value;
    }

    public function getCreatedBy()
    {
        return $this->createdBy;
    }

    public function setCreatedBy($createdBy)
    {
        $this->createdBy = $createdBy;
    }

    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;
    }

    public function getUpdatedBy()
    {
        return $this->createdBy;
    }

    public function setUdatedBy($updatedBy)
    {
        $this->updatedBy = $updatedBy;
    }

    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;
    }

    public function setCreatedAtValue()
    {
        $this->createdAt = new \DateTime();
    }

    public function setUpdatedAtValue()
    {
        $this->updatedAt = new \DateTime();
    }

}
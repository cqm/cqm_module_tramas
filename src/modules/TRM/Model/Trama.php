<?php

namespace cqm\modules\TRM\Model;

use Doctrine\ORM\Mapping as ORM;

class Trama
{

    protected $id;

    protected $idApplication;

    protected $control;

    protected $redirectionCode;

    protected $link;

    protected $expiration;

    protected $maxOpenings;

    protected $nOpenings = 0;

    protected $createdBy = 'SYSTEM';

    protected $updatedBy = '';

    protected $createdAt;

    protected $updatedAt;


    public function getId()
    {
        return $this->id;
    }

    public function getIdApplication()
    {
        return $this->idApplication;
    }

    public function setIdApplication($idApplication)
    {
        $this->idApplication = $idApplication;
    }

    public function getControl()
    {
        return $this->control;
    }

    public function setControl($control)
    {
        $this->control = $control;
    }

    public function getRedirectionCode()
    {
        return $this->redirectionCode;
    }

    public function setRedirectionCode($redirectionCode)
    {
        $this->redirectionCode = $redirectionCode;
    }

    public function getLink()
    {
        return $this->link;
    }

    public function setLink($link)
    {
        $this->link = $link;
    }

    public function getExpiration()
    {
        return $this->expiration;
    }

    public function setExpiration($expiration)
    {
        $this->expiration = $expiration;
    }

    public function getMaxOpenings()
    {
        return $this->maxOpenings;
    }

    public function setMaxOpenings($maxOpenings)
    {
        $this->maxOpenings = $maxOpenings;
    }

    public function getNOpenings()
    {
        return $this->nOpenings;
    }

    public function setNOpenings($nOpenings)
    {
        $this->nOpenings = $nOpenings;
    }

    public function getCreatedBy()
    {
        return $this->createdBy;
    }

    public function setCreatedBy($createdBy)
    {
        $this->createdBy = $createdBy;
    }

    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;
    }

    public function getUpdatedBy()
    {
        return $this->createdBy;
    }

    public function setUdatedBy($updatedBy)
    {
        $this->updatedBy = $updatedBy;
    }

    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;
    }

    public function setCreatedAtValue()
    {
        $this->createdAt = new \DateTime();
    }

    public function setUpdatedAtValue()
    {
        $this->updatedAt = new \DateTime();
    }


}
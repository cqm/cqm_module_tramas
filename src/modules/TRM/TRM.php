<?php

namespace cqm\modules\TRM;

use cqm\modules\TRM\Model\Trama;
use cqm\modules\TRM\Model\Openings;
use Doctrine\ORM\Tools\Setup;
use Doctrine\ORM\EntityManager;

class TRM
{
    static private $charset = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz123456789012345678901234567890';

    static private $connected = false;

    static private $em = null;

    const LITERAL_SUCCESS = 'success';
    const MAXIMUM_REACHED = 'maximum_reached';
    const EXPIRED_ELEMENT = 'expired_element';
    const SERVER_ERROR = 'server_error';
    const EMPTY_VALUES = 'empty_values';
    const NOT_FOUND = 'not_found';

    function __construct($conn)
    {
        self::init($conn);
    }

    static private function init($conn)
    {
        if(self::$connected){
            return;
        }

        //Create a default YAML configuration yo manage the entities
        $config = Setup::createYAMLMetadataConfiguration(array(__DIR__."/../../../config/yaml"));
        // obtaining the entity manager
        self::$em = EntityManager::create($conn, $config);

        self::$connected = true;
    }

    public static function getEntityManager()
    {
        return self::$em;
    }

    static private function ret_traveller(&$traveller, $res=0, $text_log = '', $text_user = '')
    {
        $t = json_decode($traveller, true);

        $t['Salida']['ret'] = $res;
        $t['Salida']['text_log'] = $text_log;
        $t['Salida']['text_usr'] = $text_user;

        return json_encode($t);
    }

    static public function redirect(&$traveller, $code, $end = true, $metadata = [])
    {
        $traveller = self::ret_traveller($traveller, 1, self::LITERAL_SUCCESS, self::LITERAL_SUCCESS);
        $trama = self::$em->getRepository('cqm\modules\TRM\Model\Trama')->findOneBy(
            array('redirectionCode' => $code)
        );

        if(empty($trama)){
            $traveller = self::ret_traveller($traveller, -1, self::NOT_FOUND, self::NOT_FOUND);
            return;
        }

        if($trama->getMaxOpenings() <= $trama->getNOpenings()){
            $traveller = self::ret_traveller($traveller, -1, self::SERVER_ERROR, self::SERVER_ERROR);
            return;
        }

        $now = new \DateTime('now');
        if($trama->getExpiration() <= $now){
            $traveller = self::ret_traveller($traveller, -1, self::SERVER_ERROR, self::SERVER_ERROR);
            return;
        }

        $metadata['TRM_type'] = 'REDIRECT';

        $open = new Openings();
        $open->setIdTrama($trama->getId());
        $open->setDate($now);
        $open->setMetadata(serialize($metadata));
        
        $trama->setNOpenings($trama->getNOpenings()+1);

        try {
            self::$em->persist($open);
            self::$em->persist($trama);
            self::$em->flush();
        } catch(\Exception $e) {
            $traveller = self::ret_traveller($traveller, -1, $e->getMessage(), self::SERVER_ERROR);
            return;
        }


        header('Location: '.$trama->getLink());

        if ($end) {
            die();
        }

        return;
    }

    static public function redirectionLink(&$traveller, $code, $metadata = [])
    {
        $traveller = self::ret_traveller($traveller, 1, self::LITERAL_SUCCESS, self::LITERAL_SUCCESS);
        $trama = self::$em->getRepository('cqm\modules\TRM\Model\Trama')->findOneBy(
            array('redirectionCode' => $code)
        );

        if(empty($trama)){
            $traveller = self::ret_traveller($traveller, -1, self::EMPTY_VALUES, self::EMPTY_VALUES);
            return null;
        }

        if($trama->getMaxOpenings() <= $trama->getNOpenings()){
            $traveller = self::ret_traveller($traveller, -1, self::MAXIMUM_REACHED, self::MAXIMUM_REACHED);
            return null;
        }

        $now = new \DateTime('now');
        if($trama->getExpiration() <= $now){
            $traveller = self::ret_traveller($traveller, -1, self::EXPIRED_ELEMENT, self::EXPIRED_ELEMENT);
            return null;
        }

        $metadata['TRM_type'] = 'LINK';

        $open = new Openings();
        $open->setIdTrama($trama->getId());
        $open->setDate($now);
        $open->setMetadata(serialize($metadata));

        $trama->setNOpenings($trama->getNOpenings()+1);

        try{
            self::$em->persist($open);
            self::$em->persist($trama);
            self::$em->flush();
        }catch(\Exception $e){
            $traveller = self::ret_traveller($traveller, -1, $e->getMessage(), self::SERVER_ERROR);
            return null;
        }

        return $trama->getLink();
    }

    static private function createCode($length = 10, $charset = '')
    {
        $cs = self::$charset . $charset;
        $code = '';

        for($i = 0; $i < $length; $i ++) {
            $code = $code . $cs [rand ( 0, strlen ( $cs ) )];
        }

        $sel = self::$em->getRepository('cqm\modules\TRM\Model\Trama')->findOneBy(
            array('redirectionCode' => $code)
        );

        if (!empty($sel)) {
            return self::createCode( $length, $charset );
        }

        return $code;
    }

    static public function createTrama(&$traveller, $idApplication, $link, $timeout, $maxcalls, $control, $length = 10, $charset = '')
    {
        $traveller = self::ret_traveller($traveller, 1, self::LITERAL_SUCCESS, self::LITERAL_SUCCESS);
        $code = self::createCode($length, $charset);

        $expiration = new \DateTime();
        $expiration->setTimestamp($timeout);

        $trama = new Trama();
        $trama->setRedirectionCode($code);
        $trama->setIdApplication($idApplication);
        $trama->setLink($link);
        $trama->setExpiration($expiration);
        $trama->setControl($control);
        $trama->setMaxOpenings($maxcalls);

        try {
            self::$em->persist($trama);
            self::$em->flush();
        } catch (\Exception $e) {
            $traveller = self::ret_traveller($traveller, -1, $e->getMessage(), self::SERVER_ERROR);
            return;
        }

        return $code;
    }

    /**
     * List all tramas and openings from one application. There are no entity associations as of date (09/04/2019)
     * so we'll return an array that contains both entity objects separate
     */
    public static function listTramas(&$traveller, $idApplication)
    {
        $traveller = self::ret_traveller($traveller, 1, self::LITERAL_SUCCESS, self::LITERAL_SUCCESS);
        $tramas = self::$em->getRepository(Trama::class)->findBy(array('idApplication' => $idApplication));

        return $tramas;
    }
}

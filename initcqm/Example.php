<?php

//YOUR AUTOLOAD FILE
require_once "../vendor/autoload.php";

//Import the TRM class
use cqm\modules\TRM\TRM;

//Some inicialization
$traveler = '';
$conn = array (
    'driver'    => 'pdo_mysql',
    'host'      => '127.0.0.1',
    'port'  => '8889',
    'dbname'  => 'trama',
    'user'  => 'trm',
    'password'  => 'trm',
    'charset'   => 'utf8',
    'collation' => 'utf8_unicode_ci',
);
$metadata = array(
    'test' => 1,
    'description' => 'This is the 1st test description!'
);

$time = new DateTime('now');
$time = $time->getTimestamp() + 3600;

//Call the constructor and use the functions
$trama = new TRM($conn);
$code = $trama->createTrama($traveler, 1, 'https://youtube.com', $time, 5, '', 15, '--@@__1234567890');
$trama->redirect($traveler, $code, true, $metadata);


var_dump($traveler);

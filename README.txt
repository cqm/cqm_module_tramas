Se dispone de dos archivos .sql en la carpeta initcqm/ que corresponden a la generación de la base de datos(db.sql),
y a la generacion de las tablas(tables.sql). El script de generación es opcional pero el de las tablas debe importarse
en la base de datos para que su esquema corresponda con el del módulo.
También en esta carpeta existe un archivo Example.txt que muestra un ejemplo de uso de las funciones descritas a continuación.
Una vez hecho esto no es necesario ningún paso de configuración más pero debe tenerse en cuenta que en la creación de un nuevo
objeto TRM es necesario inicializarlo pasandole un array con los parametros de conexion de la siguiente manera:

$conn = array(
    'driver'    => $driver,
    'host'      => $host,
    'port'  => $port,
    'dbname'  => $dbname,
    'user'  => $user,
    'password'  => $password,
    'charset'   => $charset,
    'collation' => $collation,
);


IMPORTANTE: Doctrine solo soporta lso siguientes drivers:
pdo_mysql, pdo_sqlite, pdo_pgsql, pdo_oci, oci8, ibm_db2, pdo_sqlsrv, mysqli, drizzle_pdo_mysql, sqlanywhere, sqlsrv



FUNCIONES:

** new TRM($conn) ---> constructor de la clase, el parametro $conn está descrito arriba

** createTrama($traveller, $idApplication, $link, $timeout, $maxcalls, $control, $length, $charset) ---->
    función encargada de crear una trama
    (string) $traveller --> recoge el resultado y los errores en la ejecución de la función
    (integer) $idApplication --> id de la aplicación a la que se asigna la trama creada
    (string)$link --> la dirección web a la que debe redireccionar el enlace
    (integer)$timeout --> fecha de expiración en formato timestamp
    (integer)$maxcalls --> número máximo de llamadas que se podrán realizar a la trama, una vez superadas el enlace expira
    (string)$control --> datos de control
    (integer)$lenght --> longitud del enlace generado para la redirección, default = 10
    (string)$charset --> conjunto de caracteres empleados para generar el código, se añadiran al charset predefinido
    RETURN devuelve el código de redirección de la trama creada

** redirect($traveller, $code, $end, $metadata) ----->
    redirige a la web correspondiente
    (string) $traveller --> recoge el resultado y los errores en la ejecución de la función
    (string) $code --> código de redirección de la trama
    (boolean) $end --> la ejecucuion del script se detiene tras la redirección, default = true
    (array) $metadata --> datos adicionales sobre la apaertura, default = []


---------------------------------------------
        VERSIONES
---------------------------------------------

1.0.1

Versión inicial


1.0.2

Cambio en las tablas Openings, Parameters y Trama: "crated_at" por "created_at"

ALTER TABLE `TRM_openings` CHANGE COLUMN `crated_at` `created_at` DATETIME NOT NULL ;
ALTER TABLE `TRM_parameters` CHANGE COLUMN `crated_at` `created_at` DATETIME NOT NULL ;
ALTER TABLE `TRM_trama` CHANGE COLUMN `crated_at` `created_at` DATETIME NOT NULL ;

